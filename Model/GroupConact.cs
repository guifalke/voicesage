﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class GroupConact
    {
        public int IdGroup { get; set; }
        public Group Group { get; set; }
        public int IdContact { get; set; }
        public Contact Contact { get; set; }
    }
}
