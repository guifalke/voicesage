﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class Contact
    {
        [Key]
        public int IdContact { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        [NotMapped]
        private Dictionary<string,string> custonFieldPArsed { get; set; }
        public string CustonField { get; set; }
        public ICollection<GroupConact> GroupConact { get; set; }
    }
}
