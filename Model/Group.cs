﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Group
    {
        [Key]
        public int IdGroup { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public  ICollection<GroupConact> GroupConact { get; set; }
    }
}
