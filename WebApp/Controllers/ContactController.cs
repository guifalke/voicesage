﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Model;

namespace WebApp.Controllers
{
    public class ContactController : Controller
    {
        public Request _request;
        public string apiBase;
        public ContactController(Request request, IConfiguration configuration)
        {
            _request = request;
            apiBase = configuration["apiUrl"] + "/api/Contact";

        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Index(int id)
        {
            Contact[] contacts =await _request.get<Contact[]>(apiBase+"/"+ id, "");
            ViewData["group"]= id;
            return View(contacts);
        }
        public async Task<IActionResult> Create(Contact contact)
        {
            await _request.put<Contact>(apiBase, "", contact);
            return RedirectToAction("Index", "Contact",new {id= contact.GroupConact.First().Group.IdGroup });
        }
        public async Task<IActionResult> Update(Contact contact)
        {
            await _request.post<Contact>(apiBase, "", contact);
            return RedirectToAction("Index", "Contact", new { id = contact.GroupConact.First().Group.IdGroup });
        }
    }
}