﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Model;

namespace WebApp.Controllers
{
    public class GroupController : Controller
    {
        public Request _request;
        public string apiBase;
        public GroupController(Request request, IConfiguration configuration)
        {
            _request = request;
            apiBase = configuration["apiUrl"]+"/api/Group";

        }

        public async Task<IActionResult> Index()
        {
            Group[] groups = await _request.get<Group[]>(apiBase,"");
            return View(groups);
        }
        public async Task<IActionResult> Create(Group group) {
            await _request.put<Group>(apiBase, "", group);
            return RedirectToAction("Index","Group");
        }
        public async Task<IActionResult> Update(Group group)
        {
            await _request.post<Group>(apiBase, "", group);
            return RedirectToAction("Index", "Group");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new  { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
