﻿using Microsoft.EntityFrameworkCore;
using Model;
using System;

namespace DB
{
    public class DBConfig : DbContext
    {
        public DbSet<Group> Group { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<GroupConact> groupConact { get; set; }
        public DBConfig(DbContextOptions options) : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GroupConact>().HasKey(sc => new { sc.IdGroup, sc.IdContact });

        }
    }
}
