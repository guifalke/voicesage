﻿using DB.Interface;
using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class ContactRepo : DBConfig, IDbActions<Contact>
    {
        public ContactRepo(DbContextOptions options) : base(options)
        {
        }

        public Task delete(Contact toDelete)
        {
            throw new NotImplementedException();
        }

        public Task<Contact> get(int ID)
        {
            return this.Contact.FirstAsync(g => g.IdContact==ID);
        }

        public Task<Contact[]> getAll()
        {
            return this.Contact.ToArrayAsync();
        }

        public Task<Contact[]> getByGroup(int id)
        {
            try
            {
                return this.Contact.Where(g => g.GroupConact.Any(u => u.IdGroup == id)).ToArrayAsync();
            }catch(Exception e)
            {
                throw;
            }
        }

        public Task insert(Contact toInsert)
        {
            this.Contact.AddAsync(toInsert).Wait();
            return this.SaveChangesAsync();
        }

        public async Task insertGroupConcat(GroupConact groupConact)
        {
            this.groupConact.AddAsync(groupConact).Wait();
            await this.SaveChangesAsync();
        }

        public Task update(Contact toUpdate)
        {
            Contact contact = get(toUpdate.IdContact).Result;
            contact.Name = toUpdate.Name;
            contact.Number = toUpdate.Number;
            contact.Email = toUpdate.Email;
            contact.CustonField = toUpdate.CustonField;
            this.Contact.Update(contact);
            return this.SaveChangesAsync();
        }
    }
}
