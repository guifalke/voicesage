﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DB.Interface
{
    public interface IDbActions<T>
    {
        Task<T[]> getAll();
        Task<T> get(int ID);
        Task insert(T toInsert);
        Task delete(T toDelete);
        Task update(T toUpdate);
        Task<T[]> getByGroup(int id);
        Task insertGroupConcat(GroupConact groupConact);
    }

}
