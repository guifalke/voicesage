﻿using DB.Interface;
using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class GroupRepo : DBConfig, IDbActions<Group>
    {
        public GroupRepo(DbContextOptions options) : base(options)
        {
        }

        public Task delete(Group toDelete)
        {
            throw new NotImplementedException();
        }

        public Task<Group> get(int ID)
        {
            return this.Group.Include(g=>g.GroupConact).FirstAsync(g=>g.IdGroup==ID);
        }

        public Task<Group[]> getAll()
        {
            return this.Group.ToArrayAsync();
        }

        public Task<Group[]> getByGroup(int id)
        {
            throw new NotImplementedException();
        }

        public Task insert(Group toInsert)
        {
            toInsert.GroupConact = new List<GroupConact>();
            this.Group.AddAsync(toInsert).Wait();
            return this.SaveChangesAsync();
        }
        public Task update(Group toUpdate)
        {
            Group group = get(toUpdate.IdGroup).Result;
            group.Description = toUpdate.Description;
            group.Name = toUpdate.Name;
            group.GroupConact = toUpdate.GroupConact;
            this.Group.Update(group);
            return this.SaveChangesAsync();

        }
        public async Task insertGroupConcat(GroupConact groupConact)
        {
            this.groupConact.AddAsync(groupConact).Wait();
            await this.SaveChangesAsync();
        }
    }
}
