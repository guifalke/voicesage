﻿using DB;
using DB.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Model;

namespace ContactApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DBConfig>(g => g.UseSqlServer(Configuration.GetConnectionString("ConnectString")));

            services.AddTransient<IDbActions<Group>, GroupRepo>();
            services.AddTransient<IDbActions<Contact>, ContactRepo>();
            services.AddCors(options =>
            {
                options.AddPolicy("all",
                builder =>
                {
                    builder.AllowAnyHeader();
                    builder.WithOrigins(Configuration["AllowedHosts"]);
                });
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<DBConfig>();

                context.Database.EnsureCreated();

                context.Database.GetAppliedMigrations();
            }

            app.UseMvc();
        }
    }
}
