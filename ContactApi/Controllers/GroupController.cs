﻿using DB;
using DB.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContactApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("all")]
    [AllowAnonymous]

    public class GroupController : ControllerBase
    {
        private readonly IDbActions<Group> _groupRepo;
        public GroupController(IDbActions<Group> groupRepo)
        {
            _groupRepo = groupRepo;
        }
        [HttpGet]
        public async Task<Group[]> Get()
        {
            return await _groupRepo.getAll();
        }
        [HttpPut]
        public async Task Put([FromBody] Group value)
        {
            await _groupRepo.insert(value);
        }
        [HttpPost]
        public async Task Post([FromBody] Group value)
        {
            await _groupRepo.update(value);
        }
    }
}