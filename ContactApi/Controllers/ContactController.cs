﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DB.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace ContactApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("all")]
    [AllowAnonymous]
    public class ContactController : ControllerBase
    {
        private readonly IDbActions<Contact> _contactRepo;
        private readonly IDbActions<Group> _groupRepo;
        public ContactController(IDbActions<Contact> contactRepo, IDbActions<Group> groupRepo)
        {
            _contactRepo = contactRepo;
            _groupRepo = groupRepo;
        }
        [HttpGet]
        public async Task<Contact[]> Get()
        {
            return await _contactRepo.getAll();
        }
        [HttpGet("{id}")]
        public async Task<Contact[]> Get(int id)
        {
            try
            {
                Contact[] contacts = await _contactRepo.getByGroup(id);
                if (contacts == null)
                    return new Contact[0];
                else
                    return contacts;
            }catch(Exception e)
            {
                throw;
            }
        }
        [HttpPut]
        public async Task Put([FromBody] Contact value)
        {
            try {
                Group group = await _groupRepo.get(value.GroupConact.First().Group.IdGroup);
                value.GroupConact = null;
                await _contactRepo.insert(value);
                await _contactRepo.insertGroupConcat(new GroupConact() {IdGroup= group.IdGroup,IdContact= value.IdContact});

            }   
            catch (Exception e)
            {
                throw;
            }
        }
        [HttpPost]
        public async Task Post([FromBody] Contact value)
        {
            try
            {
                value.GroupConact = null;
                await _contactRepo.update(value);
            }
            catch (Exception e)
            {
                throw;
            }
        }        

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
